package com.sc.tradeconfirmationsystem.utils;

public class StaticProvider {
    public static final String XML_MAKER = "XML_MAKER";
    public static final String XML_CHECKER = "XML_CHECKER";
    public static final String PDF_MAKER = "PDF_MAKER";
    public static final String PDF_CHECKER = "PDF_CHECKER";
    public static final String PDF_COMPARISON_MAKER = "PDF_COMPARISON_MAKER";
    public static final String PDF_COMPARISON_CHECKER = "PDF_COMPARISON_CHECKER";
}
